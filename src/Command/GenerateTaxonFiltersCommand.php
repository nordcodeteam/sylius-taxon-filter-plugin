<?php

namespace Nordcode\SyliusTaxonFilterPlugin\Command;

use Nordcode\SyliusTaxonFilterPlugin\Service\Generator\TaxonFilterGeneratorInterface;
use Sylius\Component\Taxonomy\Repository\TaxonRepositoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GenerateTaxonFiltersCommand extends Command
{
    protected static $defaultName = 'nordcode:taxon-filter:generate';

    private TaxonFilterGeneratorInterface $generator;
    private TaxonRepositoryInterface $taxonRepository;

    public function __construct(TaxonFilterGeneratorInterface $generator, TaxonRepositoryInterface $taxonRepository)
    {
        parent::__construct();
        $this->generator = $generator;
        $this->taxonRepository = $taxonRepository;
    }

    protected function configure()
    {
        $this
            ->setDescription('Generate taxon filters')
            ->addOption('taxon', 't', InputOption::VALUE_REQUIRED, 'Taxon id')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $taxon = null;

        if ($taxonId = $input->getOption('taxon')) {
            $taxon = $this->taxonRepository->find($taxonId);
            if ($taxon === null) {
                throw new \RuntimeException(sprintf('Taxon with id "%s" has not been found.', $taxonId));
            }
        }

        $this->generator->generate($taxon);

        $io->success('Taxon filters have been generated.');

        return 0;
    }
}
