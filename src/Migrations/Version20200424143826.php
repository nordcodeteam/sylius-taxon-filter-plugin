<?php
declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200424143826 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create table and indexes for the TaxonFilter entity.';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE nrdc_taxon_filter (id INT AUTO_INCREMENT NOT NULL, taxon_id INT DEFAULT NULL, product_attribute_id INT DEFAULT NULL, product_option_id INT DEFAULT NULL, is_enabled SMALLINT DEFAULT 0 NOT NULL, type VARCHAR(255) NOT NULL, position INT DEFAULT 0 NOT NULL, INDEX IDX_BB440D6ADE13F470 (taxon_id), INDEX IDX_BB440D6A3B420C91 (product_attribute_id), INDEX IDX_BB440D6AC964ABE2 (product_option_id), UNIQUE INDEX taxon_attribute_idx (taxon_id, product_attribute_id), UNIQUE INDEX taxon_option_idx (taxon_id, product_option_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE nrdc_taxon_filter ADD CONSTRAINT FK_BB440D6ADE13F470 FOREIGN KEY (taxon_id) REFERENCES sylius_taxon (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE nrdc_taxon_filter ADD CONSTRAINT FK_BB440D6A3B420C91 FOREIGN KEY (product_attribute_id) REFERENCES sylius_product_attribute (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE nrdc_taxon_filter ADD CONSTRAINT FK_BB440D6AC964ABE2 FOREIGN KEY (product_option_id) REFERENCES sylius_product_option (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE nrdc_taxon_filter');
    }
}
