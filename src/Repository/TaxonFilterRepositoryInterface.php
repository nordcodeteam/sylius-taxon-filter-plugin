<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Repository;

use Doctrine\ORM\QueryBuilder;
use Sylius\Component\Resource\Repository\RepositoryInterface;

interface TaxonFilterRepositoryInterface extends RepositoryInterface
{
    public function createListQueryBuilder($taxonId = null): QueryBuilder;

    public function persist(object $entity);

    public function flush(): void;

    public function clear(): void;
}
