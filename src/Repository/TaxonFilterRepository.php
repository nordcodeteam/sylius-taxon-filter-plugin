<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Repository;

use Doctrine\ORM\QueryBuilder;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class TaxonFilterRepository extends EntityRepository implements TaxonFilterRepositoryInterface
{
    public function createListQueryBuilder($taxonId = null): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('o');

        if (null !== $taxonId && 0 !== $taxonId) {
            $queryBuilder
                ->andWhere('o.taxon = :taxonId')
                ->setParameter('taxonId', $taxonId);
        }

        return $queryBuilder;
    }

    public function persist(object $entity)
    {
        $this->_em->persist($entity);
    }

    public function flush(): void
    {
        $this->_em->flush();
    }

    public function clear(): void
    {
        $this->_em->clear();
    }
}
