<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;

class ToggleStatusType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('enabled', HiddenType::class);
    }

    public function getBlockPrefix(): string
    {
        return 'app_toggle';
    }
}
