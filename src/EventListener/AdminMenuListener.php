<?php

namespace Nordcode\SyliusTaxonFilterPlugin\EventListener;

use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

final class AdminMenuListener
{
    /**
     * @param MenuBuilderEvent $event
     */
    public function addAdminMenuItems(MenuBuilderEvent $event): void
    {
        $menu = $event->getMenu();

        $menuGroup = $menu->getChild('catalog');
        $menuGroup->addChild('nordcode_sylius_taxon_filter_plugin.ui.taxon_filters', ['route' => 'nordcode_sylius_taxon_filter_plugin_admin_taxon_filter_index'])
            ->setLabelAttribute('icon', 'filter');
    }
}
