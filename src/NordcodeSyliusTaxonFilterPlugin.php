<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin;

use Nordcode\SyliusTaxonFilterPlugin\DependencyInjection\Compiler\RegisterTaxonFilterGeneratorsPass;
use Sylius\Bundle\CoreBundle\Application\SyliusPluginTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class NordcodeSyliusTaxonFilterPlugin extends Bundle
{
    use SyliusPluginTrait;

    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new RegisterTaxonFilterGeneratorsPass());
    }
}
