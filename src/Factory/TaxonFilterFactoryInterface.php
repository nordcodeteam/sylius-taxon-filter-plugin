<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Factory;

use Nordcode\SyliusTaxonFilterPlugin\Entity\TaxonFilterInterface;
use Sylius\Component\Attribute\Model\AttributeInterface;
use Sylius\Component\Core\Model\TaxonInterface;
use Sylius\Component\Product\Model\ProductOptionInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;

interface TaxonFilterFactoryInterface extends FactoryInterface
{
    public function createWithTaxonAndAttribute(TaxonInterface $taxon, AttributeInterface $attribute): TaxonFilterInterface;

    public function createWithTaxonAndOption(TaxonInterface $taxon, ProductOptionInterface $option): TaxonFilterInterface;
}
