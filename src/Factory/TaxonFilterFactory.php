<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Factory;

use Nordcode\SyliusTaxonFilterPlugin\Entity\TaxonFilterInterface;
use Sylius\Component\Attribute\Model\AttributeInterface;
use Sylius\Component\Core\Model\TaxonInterface;
use Sylius\Component\Product\Model\ProductOptionInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;

class TaxonFilterFactory implements FactoryInterface, TaxonFilterFactoryInterface
{
    private FactoryInterface $decoratedFactory;

    public function __construct(FactoryInterface $factory)
    {
        $this->decoratedFactory = $factory;
    }

    public function createNew(): TaxonFilterInterface
    {
        return $this->decoratedFactory->createNew();
    }

    public function createWithTaxonAndAttribute(TaxonInterface $taxon, AttributeInterface $attribute): TaxonFilterInterface
    {
        $taxonFilter = $this->createTyped(TaxonFilterInterface::TYPE_ATTRIBUTE);

        $taxonFilter->setTaxon($taxon);
        $taxonFilter->setAttribute($attribute);

        return $taxonFilter;
    }

    public function createWithTaxonAndOption(TaxonInterface $taxon, ProductOptionInterface $option): TaxonFilterInterface
    {
        $taxonFilter = $this->createTyped(TaxonFilterInterface::TYPE_OPTION);

        $taxonFilter->setTaxon($taxon);
        $taxonFilter->setOption($option);

        return $taxonFilter;
    }

    public function createTyped(string $type): TaxonFilterInterface
    {
        $taxonFilter = $this->createNew();
        $taxonFilter->setType($type);

        return  $taxonFilter;
    }
}
