<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Entity;

use Sylius\Component\Attribute\Model\AttributeInterface;
use Sylius\Component\Product\Model\ProductOptionInterface;

interface TaxonFilterInterface
{
    const TYPE_ATTRIBUTE = 'attribute';
    const TYPE_OPTION = 'option';

    public function getTaxon(): ?TaxonInterface;

    public function setTaxon(?TaxonInterface $taxon): void;

    public function setType(string $type): void;

    public function getType(): string;

    public function getAttribute(): ?AttributeInterface;

    public function setAttribute(?AttributeInterface $attribute): void;

    public function getOption(): ?ProductOptionInterface;

    public function setOption(?ProductOptionInterface $option): void;

    public function getPosition(): ?int;

    public function setPosition(?int $position): void;

    public function getCode(): ?string;
}
