<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Entity;

use Doctrine\Common\Collections\Collection;

interface TaxonInterface extends \Sylius\Component\Core\Model\TaxonInterface
{
    /**
     * @return Collection|TaxonFilterInterface[]
     */
    public function getTaxonFilters(): Collection;
}
