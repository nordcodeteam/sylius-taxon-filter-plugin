<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Sylius\Component\Attribute\Model\AttributeInterface;
use Sylius\Component\Product\Model\ProductOptionInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\ToggleableInterface;

/**
 * @ORM\MappedSuperclass()
 * @ORM\Table(
 *     name="nrdc_taxon_filter",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="taxon_attribute_idx", columns={"taxon_id", "product_attribute_id"}),
 *         @ORM\UniqueConstraint(name="taxon_option_idx", columns={"taxon_id", "product_option_id"})
 *     }
 * )
 */
class TaxonFilter implements TaxonFilterInterface, ToggleableInterface, ResourceInterface
{
    const TYPE_CHILDREN = 'product_type';
    const TYPE_PRICE = 'price';

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="is_enabled", type="smallint", options={"default"="0"})
     */
    protected bool $enabled = false;

    /**
     * @ORM\Column(name="type", type="string", nullable=false)
     */
    protected string $type;

    /**
     * @ORM\ManyToOne(targetEntity="Nordcode\SyliusTaxonFilterPlugin\Entity\TaxonInterface", inversedBy="taxonFilters", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="taxon_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @Gedmo\SortableGroup()
     */
    protected ?TaxonInterface $taxon;

    /**
     * @ORM\ManyToOne(targetEntity="Sylius\Component\Attribute\Model\AttributeInterface", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="product_attribute_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected ?AttributeInterface $attribute;

    /**
     * @ORM\ManyToOne(targetEntity="Sylius\Component\Product\Model\ProductOptionInterface", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="product_option_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected ?ProductOptionInterface $option;

    /**
     * @ORM\Column(name="position", type="integer", options={"default"="0"})
     *
     * @Gedmo\SortablePosition()
     */
    protected ?int $position;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(?bool $enabled): void
    {
        $this->enabled = (bool) $enabled;
    }

    public function enable(): void
    {
        $this->enabled = true;
    }

    public function disable(): void
    {
        $this->enabled = false;
    }

    public function getTaxon(): ?TaxonInterface
    {
        return $this->taxon;
    }

    public function setTaxon(?TaxonInterface $taxon): void
    {
        $this->taxon = $taxon;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getAttribute(): ?AttributeInterface
    {
        return $this->attribute;
    }

    public function setAttribute(?AttributeInterface $attribute): void
    {
        $this->attribute = $attribute;
    }

    public function getOption(): ?ProductOptionInterface
    {
        return $this->option;
    }

    public function setOption(?ProductOptionInterface $option): void
    {
        $this->option = $option;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): void
    {
        $this->position = $position;
    }

    public function getCode(): ?string
    {
        if ($this->getAttribute()) {
            return $this->getAttribute()->getCode();
        }

        if ($this->getOption()) {
            return $this->getOption()->getCode();
        }

        return null;
    }
}
