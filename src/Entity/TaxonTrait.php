<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

trait TaxonTrait
{
    /**
     * @var Collection|TaxonFilterInterface[]
     *
     * @ORM\OneToMany(
     *     targetEntity="Nordcode\SyliusTaxonFilterPlugin\Entity\TaxonFilterInterface",
     *     mappedBy="taxon",
     *     cascade={"all"},
     *     orphanRemoval=true
     * )
     *
     * @ORM\OrderBy({ "position" = "ASC" })
     */
    protected $taxonFilters;

    /**
     * @return TaxonFilterInterface[]|Collection
     */
    public function getTaxonFilters(): Collection
    {
        return $this->taxonFilters;
    }
}
