<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Service\Generator\AspectedGenerator;

use Nordcode\SyliusTaxonFilterPlugin\Entity\TaxonFilter;
use Nordcode\SyliusTaxonFilterPlugin\Entity\TaxonInterface;
use Nordcode\SyliusTaxonFilterPlugin\Service\Generator\BaseTaxonFilterGenerator;

final class PriceTaxonFilterGenerator extends BaseTaxonFilterGenerator
{
    public function generate(?TaxonInterface $taxon = null): void
    {
        foreach($this->getTaxonList($taxon) as $currentTaxon) {
            $taxonFilter = $this->taxonFilterRepository->findOneBy([
                'taxon' => $taxon,
                'type' => TaxonFilter::TYPE_PRICE,
            ]);

            if ($taxonFilter === null) {
                $taxonFilter = $this->taxonFilterFactory->createTyped(TaxonFilter::TYPE_PRICE);
                $taxonFilter->setTaxon($taxon);

                $this->persist($taxonFilter);
            }
        }
    }
}
