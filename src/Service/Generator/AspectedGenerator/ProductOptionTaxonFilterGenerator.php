<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Service\Generator\AspectedGenerator;

use Doctrine\ORM\EntityManagerInterface;
use Nordcode\SyliusTaxonFilterPlugin\Entity\TaxonInterface;
use Nordcode\SyliusTaxonFilterPlugin\Factory\TaxonFilterFactoryInterface;
use Nordcode\SyliusTaxonFilterPlugin\Repository\TaxonFilterRepositoryInterface;
use Nordcode\SyliusTaxonFilterPlugin\Service\Generator\BaseTaxonFilterGenerator;
use Sylius\Component\Product\Model\ProductOptionInterface;
use Sylius\Component\Product\Repository\ProductOptionRepositoryInterface;
use Sylius\Component\Taxonomy\Repository\TaxonRepositoryInterface;

final class ProductOptionTaxonFilterGenerator extends BaseTaxonFilterGenerator
{
    private ProductOptionRepositoryInterface $productOptionRepository;

    public function __construct(
        TaxonFilterFactoryInterface $taxonFilterFactory,
        TaxonFilterRepositoryInterface $taxonFilterRepository,
        TaxonRepositoryInterface $taxonRepository,
        EntityManagerInterface $entityManager,
        ProductOptionRepositoryInterface $productOptionRepository
    ) {
        parent::__construct($taxonFilterFactory, $taxonFilterRepository, $taxonRepository, $entityManager);

        $this->productOptionRepository = $productOptionRepository;
    }

    public function generate(?TaxonInterface $taxon = null): void
    {
        /** @var ProductOptionInterface[] $options */
        $options = $this->productOptionRepository->findAll();

        foreach($this->getTaxonList($taxon) as $currentTaxon) {
            foreach ($options as $option) {
                $this->generateOptionFilterForTaxon($currentTaxon, $option);
            }
        }
    }

    private function generateOptionFilterForTaxon(TaxonInterface $taxon, ProductOptionInterface $option): void
    {
        $taxonFilter = $this->taxonFilterRepository->findOneBy([
            'taxon' => $taxon,
            'option' => $option,
        ]);

        if ($taxonFilter === null) {
            $taxonFilter = $this->taxonFilterFactory->createWithTaxonAndOption($taxon, $option);
            $this->persist($taxonFilter);
        }
    }
}
