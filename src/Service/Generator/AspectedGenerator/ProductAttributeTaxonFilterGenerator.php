<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Service\Generator\AspectedGenerator;

use Doctrine\ORM\EntityManagerInterface;
use Nordcode\SyliusTaxonFilterPlugin\Entity\TaxonInterface;
use Nordcode\SyliusTaxonFilterPlugin\Factory\TaxonFilterFactoryInterface;
use Nordcode\SyliusTaxonFilterPlugin\Repository\TaxonFilterRepositoryInterface;
use Nordcode\SyliusTaxonFilterPlugin\Service\Generator\BaseTaxonFilterGenerator;
use Sylius\Component\Product\Model\ProductAttributeInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Sylius\Component\Taxonomy\Repository\TaxonRepositoryInterface;

final class ProductAttributeTaxonFilterGenerator extends BaseTaxonFilterGenerator
{
    private RepositoryInterface $productAttributeRepository;

    public function __construct(
        TaxonFilterFactoryInterface $taxonFilterFactory,
        TaxonFilterRepositoryInterface $taxonFilterRepository,
        TaxonRepositoryInterface $taxonRepository,
        EntityManagerInterface $entityManager,
        RepositoryInterface $productAttributeRepository
    ) {
        parent::__construct($taxonFilterFactory, $taxonFilterRepository, $taxonRepository, $entityManager);

        $this->productAttributeRepository = $productAttributeRepository;
    }

    public function generate(?TaxonInterface $taxon = null): void
    {
        /** @var ProductAttributeInterface[] $attributes */
        $attributes = $this->productAttributeRepository->findAll();

        foreach($this->getTaxonList($taxon) as $currentTaxon) {
            foreach ($attributes as $attribute) {
                $this->generateAttributeFilterForTaxon($currentTaxon, $attribute);
            }
        }
    }

    private function generateAttributeFilterForTaxon(TaxonInterface $taxon, ProductAttributeInterface $attribute): void
    {
        $taxonFilter = $this->taxonFilterRepository->findOneBy([
            'taxon' => $taxon,
            'attribute' => $attribute,
        ]);

        if ($taxonFilter === null) {
            $taxonFilter = $this->taxonFilterFactory->createWithTaxonAndAttribute($taxon, $attribute);

            $this->persist($taxonFilter);
        }
    }
}
