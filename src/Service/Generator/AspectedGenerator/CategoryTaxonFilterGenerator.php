<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Service\Generator\AspectedGenerator;

use Nordcode\SyliusTaxonFilterPlugin\Entity\TaxonFilter;
use Nordcode\SyliusTaxonFilterPlugin\Entity\TaxonInterface;
use Nordcode\SyliusTaxonFilterPlugin\Service\Generator\BaseTaxonFilterGenerator;

final class CategoryTaxonFilterGenerator extends BaseTaxonFilterGenerator
{
    public function generate(?TaxonInterface $taxon = null): void
    {
        foreach($this->getTaxonList($taxon) as $currentTaxon) {
            $taxonFilter = $this->taxonFilterRepository->findOneBy([
                'taxon' => $currentTaxon,
                'type' => TaxonFilter::TYPE_CHILDREN,
            ]);

            if ($taxonFilter === null) {
                $taxonFilter = $this->taxonFilterFactory->createTyped(TaxonFilter::TYPE_CHILDREN);
                $taxonFilter->setTaxon($currentTaxon);

                $this->persist($taxonFilter);
            }
        }
    }
}
