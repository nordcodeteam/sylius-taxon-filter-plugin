<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Service\Generator;

use Doctrine\ORM\EntityManagerInterface;
use Nordcode\SyliusTaxonFilterPlugin\Entity\TaxonFilterInterface;
use Nordcode\SyliusTaxonFilterPlugin\Entity\TaxonInterface;
use Nordcode\SyliusTaxonFilterPlugin\Factory\TaxonFilterFactoryInterface;
use Nordcode\SyliusTaxonFilterPlugin\Repository\TaxonFilterRepositoryInterface;
use Sylius\Component\Taxonomy\Repository\TaxonRepositoryInterface;

final class CompositeTaxonFilterGenerator extends BaseTaxonFilterGenerator implements CompositeTaxonFilterGeneratorInterface
{
    /** @var TaxonFilterGeneratorInterface[] */
    private array $generators = [];

    private EntityManagerInterface $entityManager;

    public function __construct(
        TaxonFilterFactoryInterface $taxonFilterFactory,
        TaxonFilterRepositoryInterface $taxonFilterRepository,
        TaxonRepositoryInterface $taxonRepository,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct($taxonFilterFactory, $taxonFilterRepository, $taxonRepository, $entityManager);

        $this->entityManager = $entityManager;
    }

    public function addGenerator(TaxonFilterGeneratorInterface $generator): void
    {
        $this->generators[] = $generator;
    }

    public function generate(?TaxonInterface $taxon = null): void
    {
        $taxons = $this->getTaxonList($taxon);

        foreach ($taxons as $currentTaxon) {
            // Prefetch existing TaxonFilters
            $this->taxonFilterRepository->findBy(['taxon' => $taxon]);

            foreach ($this->generators as $generator) {
                $generator->generate($currentTaxon);
            }

            $this->entityManager->clear(TaxonFilterInterface::class);
        }
    }
}
