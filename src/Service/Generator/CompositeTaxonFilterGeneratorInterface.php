<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Service\Generator;

interface CompositeTaxonFilterGeneratorInterface extends TaxonFilterGeneratorInterface
{
    public function addGenerator(TaxonFilterGeneratorInterface $generator): void;
}
