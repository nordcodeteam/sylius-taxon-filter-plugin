<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Service\Generator;

use Doctrine\ORM\EntityManagerInterface;
use Nordcode\SyliusTaxonFilterPlugin\Entity\TaxonFilterInterface;
use Nordcode\SyliusTaxonFilterPlugin\Entity\TaxonInterface;
use Nordcode\SyliusTaxonFilterPlugin\Factory\TaxonFilterFactoryInterface;
use Nordcode\SyliusTaxonFilterPlugin\Repository\TaxonFilterRepositoryInterface;
use Sylius\Component\Taxonomy\Repository\TaxonRepositoryInterface;

abstract class BaseTaxonFilterGenerator implements TaxonFilterGeneratorInterface
{
    protected TaxonFilterFactoryInterface $taxonFilterFactory;
    protected TaxonFilterRepositoryInterface $taxonFilterRepository;
    private TaxonRepositoryInterface $taxonRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(
        TaxonFilterFactoryInterface $taxonFilterFactory,
        TaxonFilterRepositoryInterface $taxonFilterRepository,
        TaxonRepositoryInterface $taxonRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->taxonFilterFactory = $taxonFilterFactory;
        $this->taxonRepository = $taxonRepository;
        $this->taxonFilterRepository = $taxonFilterRepository;
        $this->entityManager = $entityManager;
    }

    protected function getTaxonList(?TaxonInterface $taxon = null): array
    {
        return $taxon ? [$taxon] : $this->taxonRepository->findAll();
    }

    protected function persist(TaxonFilterInterface $taxonFilter): void
    {
        $this->entityManager->persist($taxonFilter);
        $this->entityManager->flush();
    }
}
