<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Service\Generator;

use Nordcode\SyliusTaxonFilterPlugin\Entity\TaxonInterface;

interface TaxonFilterGeneratorInterface
{
    public function generate(?TaxonInterface $taxon = null): void;
}
