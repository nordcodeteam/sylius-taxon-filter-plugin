<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Service\Provider;

use Nordcode\SyliusTaxonFilterPlugin\Entity\TaxonInterface;

interface TaxonFilterProviderInterface
{
    public function getAvailableFilters(TaxonInterface $taxon): array;
}
