<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Service\Provider;

use Nordcode\SyliusTaxonFilterPlugin\Entity\TaxonFilter;
use Nordcode\SyliusTaxonFilterPlugin\Entity\TaxonInterface;

class TaxonFilterProvider implements TaxonFilterProviderInterface
{
    public function getAvailableFilters(TaxonInterface $taxon): array
    {
        $filters = [];

        /** @var TaxonFilter $taxonFilter */
        foreach ($taxon->getTaxonFilters()->toArray() as $taxonFilter) {
            if (true === $taxonFilter->isEnabled()) {
                $filters[] = $taxonFilter->getCode();
            }
        }

        if (count($filters) === 0 && $taxon->getParent() !== null) {
            return $this->getAvailableFilters($taxon->getParent());
        }

        $result = [];

        /** @var TaxonFilter $filter */
        foreach ($filters as $filter) {
            $result[] = [
                'code' => $filter->getCode(),
                'position' => $filter->getPosition(),
                'type' => $filter->getType(),
            ];
        }

        return $result;
    }
}
