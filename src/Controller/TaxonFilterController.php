<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\Controller;

use Nordcode\SyliusTaxonFilterPlugin\Service\Generator\TaxonFilterGeneratorInterface;
use Gedmo\Sortable\Sortable;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController as BaseResourceController;
use Sylius\Component\Core\Model\TaxonInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\ResourceActions;
use Sylius\Component\Taxonomy\Repository\TaxonRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\RouterInterface;

class TaxonFilterController extends BaseResourceController
{
    public function updatePositionsAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);
        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);
        $positions = $request->get('positions');

        if ($configuration->isCsrfProtectionEnabled() && !$this->isCsrfTokenValid('update-position', $request->request->get('_csrf_token'))) {
            throw new HttpException(Response::HTTP_FORBIDDEN, 'Invalid csrf token.');
        }

        if (in_array($request->getMethod(), ['POST', 'PUT', 'PATCH'], true) && null !== $positions) {
            foreach ($positions as $position) {
                if (!is_numeric($position['position'])) {
                    throw new HttpException(
                        Response::HTTP_BAD_REQUEST,
                        sprintf('The position "%s" is invalid.', $position['position'])
                    );
                }

                /** @var ResourceInterface | Sortable $resource */
                $resource = $this->repository->findOneBy(['id' => $position['id']]);
                $resource->setPosition((int) $position['position']);

                $this->manager->flush();
            }
        }

        return new JsonResponse();
    }

    public function generateAction(
        Request $request,
        TaxonFilterGeneratorInterface $taxonFilterGenerator,
        RouterInterface $router,
        TaxonRepositoryInterface $taxonRepository
    ): Response {
        $taxonId = $request->query->get('taxonId');

        if ($taxonId) {
            /** @var TaxonInterface|null $taxon */
            $taxon = $taxonRepository->find($taxonId);
            $redirectOptions = [
                'taxonId' => $taxon->getId(),
            ];
        }

        $taxonFilterGenerator->generate(isset($taxon) ? $taxon : null);

        return new RedirectResponse(
            $router->generate(
                'nordcode_sylius_taxon_filter_plugin_admin_taxon_filter_per_taxon_index',
                isset($redirectOptions) ? $redirectOptions : []
            )
        );
    }
}
