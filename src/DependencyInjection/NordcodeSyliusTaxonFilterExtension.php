<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\DependencyInjection;

use Nordcode\SyliusTaxonFilterPlugin\Service\Generator\TaxonFilterGeneratorInterface;
use Sylius\Bundle\ResourceBundle\DependencyInjection\Extension\AbstractResourceExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;

final class NordcodeSyliusTaxonFilterExtension extends AbstractResourceExtension
{
    public const GENERATOR_TAG_NAME = 'nordcode_sylius_taxon_filter_plugin.generator';

    public function load(array $config, ContainerBuilder $container): void
    {
        $container->registerForAutoconfiguration(TaxonFilterGeneratorInterface::class)
            ->addTag(self::GENERATOR_TAG_NAME);
    }
}
