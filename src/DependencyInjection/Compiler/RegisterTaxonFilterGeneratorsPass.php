<?php
declare(strict_types=1);

namespace Nordcode\SyliusTaxonFilterPlugin\DependencyInjection\Compiler;

use Nordcode\SyliusTaxonFilterPlugin\DependencyInjection\NordcodeSyliusTaxonFilterExtension;
use Nordcode\SyliusTaxonFilterPlugin\Service\Generator\CompositeTaxonFilterGeneratorInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

final class RegisterTaxonFilterGeneratorsPass implements CompilerPassInterface
{
    private const COMPOSITE_GENERATOR_SERVICE_ID = 'nordcode_sylius_taxon_filter_plugin.generator.composite';

    public function process(ContainerBuilder $container): void
    {
        if (!$container->has(self::COMPOSITE_GENERATOR_SERVICE_ID)) {
            return;
        }

        $compositeGeneratorDefinition = $container->findDefinition(self::COMPOSITE_GENERATOR_SERVICE_ID);
        $implementedInterfaces = class_implements($compositeGeneratorDefinition->getClass());

        if (!in_array(CompositeTaxonFilterGeneratorInterface::class, $implementedInterfaces)) {
            throw new \RuntimeException(sprintf(
                'Service "%s" must implement %s.',
                self::COMPOSITE_GENERATOR_SERVICE_ID,
                CompositeTaxonFilterGeneratorInterface::class
            ));
        }

        $taggedServices = $container->findTaggedServiceIds(NordcodeSyliusTaxonFilterExtension::GENERATOR_TAG_NAME);

        foreach ($taggedServices as $id => $tags) {
            if ($id === self::COMPOSITE_GENERATOR_SERVICE_ID) {
                continue;
            }

            if (!$container->hasParameter($id) || false !== $container->getParameter($id)) {
                $compositeGeneratorDefinition->addMethodCall('addGenerator', [new Reference($id)]);
            }
        }
    }
}
